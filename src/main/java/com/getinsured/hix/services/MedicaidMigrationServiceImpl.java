package com.getinsured.hix.services;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;

import com.getinsured.hix.DTO.Applicant;
import com.getinsured.hix.DTO.MedicaidOutbound;
import com.getinsured.hix.DTO.MedicaidOutboundApplicant;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeType;
import com.opencsv.CSVWriter;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MedicaidMigrationServiceImpl {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Value("${sql.outbound.fetch.query}")
	private String applicantFetchQuery;
	
	@Value("${sql.outbound.payload.fetch.query}")
	private String payloadFetchQuery;
	
	@Value("${sql.outbound.save.query}")
	private String outboundSaveQuery;
	
	@Value("${sql.outbound.applicant.save.query}")
	private String outboundApplicantSaveQuery;
	

	public void doMigration() {
		log.info("Migration process is started....");
		Instant start = Instant.now();
		
		log.info("applicantFetchQuery : " + applicantFetchQuery);
		
		// Load the data from the old table
		List<Applicant> applicantList = jdbcTemplate.query(
				applicantFetchQuery,
				(rs, rowNum) -> new Applicant(rs.getLong("ssap_application_id"), rs.getString("applicant_guid"), rs.getInt("outbound_at_payload_id"), rs.getDate("date_sent"))
		);
		log.info("Total migration Records size : " + applicantList.size());
		
		CSVWriter writer = null;
		try {
			String fileName ="Report-" + new SimpleDateFormat("yyyyMMddHHmm'.csv'").format(new Date());
		    writer = new CSVWriter(new FileWriter(fileName,true));
		    writer.writeNext("SsapApplicationId,GiWsPayloadId,ApplicantGuid,Status".split(","));
		    
		    // Add extra information and save the data
		    int counter = 0;
			Map<String, MedicaidOutbound> medicaidOutbounds = new HashMap<String, MedicaidOutbound>();
			for(Applicant applicant : applicantList) {
				String groupByKey = applicant.getSsapApplicationId() + "-" +applicant.getGiWsPayloadId();				
				try {
					MedicaidOutbound medicaidOutbound = medicaidOutbounds.get(groupByKey);
					
					if(medicaidOutbound == null) {
						medicaidOutbound = createMedicaidOutbound(applicant);
					}
					
					medicaidOutbound.addMedicaidOutboundApplicant(createMedicaidOutboundApplicant(applicant, medicaidOutbound));
					
					medicaidOutbounds.put(groupByKey, medicaidOutbound);
					writer.writeNext(applicant.getDataToWriteForSuccess());
					
					counter++;
					if((counter%100) == 0) {
						log.info("Number of records are processed : "+counter + " , Still It is in progress");
					}
				} catch (Exception e) {
					log.error("Error accured while processing SsapApplicationId : " + applicant.getSsapApplicationId() + ", GiWsPayloadId : "+applicant.getGiWsPayloadId());
					log.error("Error : ",e);
					writer.writeNext(applicant.getDataToWriteForFailure(e));
				}
			}
			
			log.info("Migration process is completed....");
		} catch (Exception e) {
			log.error("Error accured while Migration process : ",e);
		}
		finally {
			if(writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}
		}
		
		Instant end = Instant.now();
		Duration timeElapsed = Duration.between(start, end);
		log.info("Time taken: "+ timeElapsed.toMillis() +"  Milliseconds");
	}
	
	private MedicaidOutbound createMedicaidOutbound(Applicant applicant) throws Exception {
		//log.info("Processing the application : "+applicant.getSsapApplicationId() + " - " +applicant.getGiWsPayloadId());
		
		MedicaidOutbound medicaidOutbound = new MedicaidOutbound();

		medicaidOutbound.setSsapApplicationId(applicant.getSsapApplicationId());
		medicaidOutbound.setStatus("TRANSMITTED");
		medicaidOutbound.setGiWsPayloadId(applicant.getGiWsPayloadId());
		medicaidOutbound.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		if (applicant.getDateSent() != null) {
			medicaidOutbound.setCreatedOn(new Timestamp(applicant.getDateSent().getTime()));
		}
		
		AccountTransferRequestPayloadType request = payload(medicaidOutbound.getGiWsPayloadId());
		if(request != null) {
			String transferId =  request.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue();	
			medicaidOutbound.setTransferId(transferId);
			
			String householdCaseId =  request.getInsuranceApplication().getApplicationIdentification().get(0).getIdentificationID()	.getValue();
			medicaidOutbound.setHouseholdCaseId(householdCaseId);
			
			medicaidOutbound.setRequest(request);
		}
		
		medicaidOutbound.setFullDetermination(false);
		
		saveMedicaidOutbound(medicaidOutbound);
		
		return medicaidOutbound;
	}

	private MedicaidOutboundApplicant createMedicaidOutboundApplicant(Applicant applicant, MedicaidOutbound medicaidOutbound) {
		//log.info("Processing the applicant : "+applicant.getApplicantGuid());
		
		MedicaidOutboundApplicant medicaidOutboundApplicant = new MedicaidOutboundApplicant();

		medicaidOutboundApplicant.setMedicaidOutBoundId(medicaidOutbound.getMedicaidOutboundId());
		medicaidOutboundApplicant.setApplicantGuid(applicant.getApplicantGuid());
		medicaidOutboundApplicant.setOutboundStatus("TRANSMITTED");
		medicaidOutboundApplicant.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		if (applicant.getDateSent() != null) {
			medicaidOutboundApplicant.setCreatedOn(new Timestamp(applicant.getDateSent().getTime()));
		}

		if(isMemberEligibleForMedicaid(applicant, medicaidOutbound)) {
			medicaidOutboundApplicant.setEligibilityStatus("INITIATED");	
		}
		else {
			medicaidOutboundApplicant.setEligibilityStatus("NONE");
		}
		
		saveMedicaidOutboundApplicant(medicaidOutboundApplicant);
		
		return medicaidOutboundApplicant;
	}
	
	private boolean isMemberEligibleForMedicaid(Applicant applicant, MedicaidOutbound medicaidOutbound) {
		
		if(medicaidOutbound.getRequest() == null) 
			return false;
		
		List<InsuranceApplicantType> insuranceApplicantList = medicaidOutbound.getRequest().getInsuranceApplication().getInsuranceApplicant();
		
		for (InsuranceApplicantType insuranceApplicant : insuranceApplicantList) {
			
			if (insuranceApplicant.getReferralActivity() != null && insuranceApplicant.getReferralActivity().getReferralActivityStatus() != null) {
				ReferralActivityStatusCodeType referralActivityStatus = insuranceApplicant.getReferralActivity().getReferralActivityStatus().getReferralActivityStatusCode();
				
				if (referralActivityStatus != null && referralActivityStatus.getValue() != null && StringUtils.isNotBlank(referralActivityStatus.getValue().value())) {
					
					String applicantGuid = insuranceApplicant.getReferralActivity().getActivityIdentification().getIdentificationID().getValue();

					if(!applicantGuid.equals(applicant.getApplicantGuid())){
						continue;
					}
					
					if (ReferralActivityStatusCodeSimpleType.INITIATED.equals(referralActivityStatus.getValue())) {
						return true;
					} else {
						return false;
					}
					
				}
			
			}
		
		}
		
		return false;
	}

	private AccountTransferRequestPayloadType payload(Integer giWsPayloadId) throws Exception {
		// Load the data from the gi_ws_payload table
		List<String> payload = jdbcTemplate.query(
				payloadFetchQuery,
				new Object[] { giWsPayloadId },
				(rs, rowNum) -> rs.getString("request_payload")
		);
		
		String payloadData = payload.get(0);
		
		return unmarshal(payloadData);
	}
	
	public static AccountTransferRequestPayloadType unmarshal(String request) throws Exception {
		AccountTransferRequestPayloadType payload = null;
		InputStream is = null;
		try {
			is = IOUtils.toInputStream(request, "UTF-8");
			payload = (AccountTransferRequestPayloadType) inputStreamToObject(is, AccountTransferRequestPayloadType.class);
		} catch (Exception e) {
			log.error("Error occurred while unmarshalling request string - ", e);
			throw e;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e2) {
					log.error(e2.getMessage());
				}
			}

		}
		return payload;
	}

	public static Object inputStreamToObject(InputStream is, Class<?> c) throws JAXBException, XMLStreamException {
		JAXBContext jc = JAXBContext.newInstance(c);

		XMLInputFactory xif = XMLInputFactory.newInstance();
		xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
		xif.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		XMLStreamReader xsr = xif.createXMLStreamReader(is);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		return unmarshaller.unmarshal(xsr);
	}
	
	void saveMedicaidOutbound(MedicaidOutbound medicaidOutbound) {
		
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		String id_column = "id";
		
	    jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(outboundSaveQuery, new String[]{id_column});
	          ps.setLong(1, medicaidOutbound.getSsapApplicationId());
	          ps.setString(2, medicaidOutbound.getStatus());
	          ps.setLong(3, medicaidOutbound.getGiWsPayloadId());
	          ps.setTimestamp(4, medicaidOutbound.getUpdatedOn());
	          ps.setTimestamp(5, medicaidOutbound.getCreatedOn());
	          ps.setString(6, medicaidOutbound.getTransferId());
	          ps.setBoolean(7, medicaidOutbound.isFullDetermination());
	          ps.setString(8, medicaidOutbound.getHouseholdCaseId());
	          return ps;
	        }, keyHolder);
	 
	    BigDecimal id = (BigDecimal) keyHolder.getKeys().get(id_column);
	    Long medicaidOutboundId = id.longValue();
	    
		medicaidOutbound.setMedicaidOutboundId(medicaidOutboundId);
	}
	
	void saveMedicaidOutboundApplicant(MedicaidOutboundApplicant medicaidOutboundApplicant) {
		jdbcTemplate.update(
			    outboundApplicantSaveQuery,
			    medicaidOutboundApplicant.getApplicantGuid(), medicaidOutboundApplicant.getMedicaidOutBoundId(), medicaidOutboundApplicant.getOutboundStatus(), 
			    medicaidOutboundApplicant.getUpdatedOn(), medicaidOutboundApplicant.getCreatedOn(), medicaidOutboundApplicant.getEligibilityStatus()
		);
	}
	
	
}