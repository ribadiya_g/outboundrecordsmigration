package com.getinsured.hix.DTO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

public class MedicaidOutbound {

	private Long medicaidOutboundId;
	private Long ssapApplicationId;
	private String status;
	private Integer giWsPayloadId;
	private AccountTransferRequestPayloadType request;
	private Timestamp updatedOn;
	private Timestamp createdOn;
	private String transferId;
	private boolean fullDetermination;
	private String householdCaseId;
	private List<MedicaidOutboundApplicant> applicants;

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getTransferId() {
		return transferId;
	}

	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}

	public boolean isFullDetermination() {
		return fullDetermination;
	}

	public void setFullDetermination(boolean fullDetermination) {
		this.fullDetermination = fullDetermination;
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}

	public List<MedicaidOutboundApplicant> getApplicants() {
		return applicants;
	}

	public void setApplicants(List<MedicaidOutboundApplicant> applicants) {
		this.applicants = applicants;
	}

	public void addMedicaidOutboundApplicant(MedicaidOutboundApplicant medicaidOutboundApplicant) {
		if (this.applicants == null) {
			this.applicants = new ArrayList<MedicaidOutboundApplicant>();
		}
		this.applicants.add(medicaidOutboundApplicant);
	}

	public Long getMedicaidOutboundId() {
		return medicaidOutboundId;
	}

	public void setMedicaidOutboundId(Long medicaidOutboundId) {
		this.medicaidOutboundId = medicaidOutboundId;
	}
	
	public AccountTransferRequestPayloadType getRequest() {
		return request;
	}

	public void setRequest(AccountTransferRequestPayloadType request) {
		this.request = request;
	}

	@Override
	public String toString() {
		return "MedicaidOutbound [ssapApplicationId=" + ssapApplicationId + ", status=" + status + ", giWsPayloadId="
				+ giWsPayloadId + ", updatedOn=" + updatedOn + ", createdOn=" + createdOn + ", transferId=" + transferId
				+ ", fullDetermination=" + fullDetermination + ", householdCaseId=" + householdCaseId + ", applicants="
				+ applicants + "]";
	}
}
