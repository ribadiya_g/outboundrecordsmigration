package com.getinsured.hix.DTO;

import java.util.Date;

public class Applicant {
	private Long ssapApplicationId;
	private String applicantGuid;
	private Integer giWsPayloadId;
	private Date dateSent;

	public Applicant() {

	}

	public Applicant(Long ssapApplicationId, String applicantGuid, Integer giWsPayloadId, Date dateSent) {
		super();
		this.ssapApplicationId = ssapApplicationId;
		this.applicantGuid = applicantGuid;
		this.giWsPayloadId = giWsPayloadId;
		this.dateSent = dateSent;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public Date getDateSent() {
		return dateSent;
	}

	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}
	
	public String [] getDataToWriteForSuccess() {
		String [] record = (ssapApplicationId + "," + giWsPayloadId + "," + applicantGuid + "," + "Success").split(",");
		return record;
	}
	
	public String [] getDataToWriteForFailure(Exception e) {
		String [] record = (ssapApplicationId + "," + giWsPayloadId + "," + applicantGuid + "," + e.toString()).split(",");
		return record;
	}

	@Override
	public String toString() {
		return "Applicant [ssapApplicationId=" + ssapApplicationId + ", applicantGuid=" + applicantGuid
				+ ", giWsPayloadId=" + giWsPayloadId + ", dateSent=" + dateSent + "]";
	}
}
