package com.getinsured.hix.DTO;

import java.sql.Timestamp;

public class MedicaidOutboundApplicant {

	private String applicantGuid;
	private Long medicaidOutBoundId;
	private String outboundStatus;
	private Timestamp updatedOn;
	private Timestamp createdOn;
	private String eligibilityStatus;

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public String getOutboundStatus() {
		return outboundStatus;
	}

	public void setOutboundStatus(String outboundStatus) {
		this.outboundStatus = outboundStatus;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public Long getMedicaidOutBoundId() {
		return medicaidOutBoundId;
	}

	public void setMedicaidOutBoundId(Long medicaidOutBoundId) {
		this.medicaidOutBoundId = medicaidOutBoundId;
	}
	
	@Override
	public String toString() {
		return "MedicaidOutboundApplicant [applicantGuid=" + applicantGuid + ", outboundStatus=" + outboundStatus
				+ ", updatedOn=" + updatedOn + ", createdOn=" + createdOn + ", eligibilityStatus=" + eligibilityStatus
				+ "]";
	}
}
