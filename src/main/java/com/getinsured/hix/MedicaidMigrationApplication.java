package com.getinsured.hix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.getinsured.hix.services.MedicaidMigrationServiceImpl;

@SpringBootApplication
public class MedicaidMigrationApplication {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(MedicaidMigrationApplication.class, args);
		MedicaidMigrationServiceImpl medicaidMigrationServiceImpl = context.getBean(MedicaidMigrationServiceImpl.class);

		medicaidMigrationServiceImpl.doMigration();

	}

}
